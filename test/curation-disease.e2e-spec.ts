import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { getModelToken } from '@nestjs/mongoose';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let Disease;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
    Disease = app.get(getModelToken('Disease'));
  });

  describe('POST /curation/diseases', () => {
    beforeEach(async () => {
      await Disease.create({ name: 'Foo', actions: [] });
    });

    afterEach(async () => {
      await Disease.deleteMany({});
    });

    it('gets a BadRequest when called without name', async () => {
      const {
        body: { statusCode },
      } = await request(app.getHttpServer()).post('/curation/diseases');

      expect(statusCode).toBe(400);
    });

    it('gets a BadRequest when called with duplicated name', async () => {
      const {
        body: { statusCode },
      } = await request(app.getHttpServer())
        .post('/curation/diseases')
        .send({ name: 'Foo' });

      expect(statusCode).toBe(400);
    });

    it('gets a disease object when called with right arguments', async () => {
      const {
        body: { _id },
      } = await request(app.getHttpServer())
        .post('/curation/diseases')
        .send({ name: 'Osteoporose' });

      expect(_id).toBeDefined();
    });
  });
});
