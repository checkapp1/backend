import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';

const db = [
  {
    name: 'Calculadora FRAX',
  },
];

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let Wiki;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    Wiki = app.get(getModelToken('Wiki'));
    await app.init();

    await Wiki.insertMany(db);
  });

  afterEach(async () => {
    await Wiki.deleteMany({});
  });

  describe('GET /mobile/wikis', () => {
    it('gets a list of wikis, with name and id', async () => {
      const { body } = await request(app.getHttpServer()).get('/mobile/wikis');

      expect(body).toHaveLength(1);
    });
  });
});
