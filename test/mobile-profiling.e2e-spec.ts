import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });

  describe('GET /mobile/profiling', () => {
    it('gets a BadRequest when called without age', async () => {
      const {
        body: { statusCode, error },
      } = await request(app.getHttpServer())
        .get('/mobile/profiling')
        .query({ sex: 'other' });

      expect(statusCode).toBe(400);
      expect(error).toEqual('Bad Request');
    });

    it('gets a BadRequest when called without sex', async () => {
      const {
        body: { statusCode, error },
      } = await request(app.getHttpServer())
        .get('/mobile/profiling')
        .query({ age: 81 });

      expect(statusCode).toBe(400);
      expect(error).toEqual('Bad Request');
    });

    it('gets a Bad Request when called with unknown sex', async () => {
      const {
        body: { statusCode, error },
      } = await request(app.getHttpServer())
        .get('/mobile/profiling')
        .query({ age: 81, sex: 'foo' });

      expect(statusCode).toBe(400);
      expect(error).toEqual('Bad Request');
    });

    it('gets a Bad Request when called with out-of-range age', async () => {
      const {
        body: { statusCode, error },
      } = await request(app.getHttpServer())
        .get('/mobile/profiling')
        .query({ age: -1, sex: 'female' });

      expect(statusCode).toBe(400);
      expect(error).toEqual('Bad Request');
    });

    it('succeeds when called with all right arguments', async () => {
      const { body } = await request(app.getHttpServer())
        .get('/mobile/profiling')
        .query({ age: 45, sex: 'female' });

      expect(body.error).toBeUndefined();
    });
  });
});
