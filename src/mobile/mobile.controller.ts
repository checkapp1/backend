import { Controller, Get, Query, Param } from '@nestjs/common';
import { DiseaseService } from '../checkapp/diseases.service';
import { ProfilingQuery } from './dto/profiling_query.dto';
import { WikiService } from '../checkapp/wikis.service';

@Controller('mobile')
export class MobileController {
  constructor(
    private diseasesSVC: DiseaseService,
    private wikiSVC: WikiService,
  ) {}

  @Get('/profiling')
  async profiling(@Query() dto: ProfilingQuery) {
    return this.diseasesSVC.filter(dto);
  }

  @Get('/wikis')
  async allWikis() {
    return this.wikiSVC.allNames();
  }

  @Get('/wikis/:id')
  async findWiki(@Param('id') id) {
    return await this.wikiSVC.findOne(id);
  }
}
