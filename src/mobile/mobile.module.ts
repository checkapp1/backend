import { Module } from '@nestjs/common';
import { MobileController } from './mobile.controller';
import { CheckappModule } from '../checkapp/checkapp.module';

@Module({
  imports: [CheckappModule],
  controllers: [MobileController],
})
export class MobileModule {}
