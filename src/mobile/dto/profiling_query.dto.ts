import {
  IsString,
  IsInt,
  Min,
  Max,
  Matches,
  IsNumberString,
} from 'class-validator';

export class ProfilingQuery {
  @IsString()
  @Matches(/(male|female)/)
  readonly sex: string;

  @IsNumberString()
  @Matches(/^([1-9]?[0-9]|1[0-1][0-9])$/)
  readonly age: number;
}
