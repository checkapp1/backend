import { Test } from '@nestjs/testing';
import { MobileController } from './mobile.controller';
import { CheckappModule } from '../checkapp/checkapp.module';
import { AppModule } from '../app.module';
import { DiseaseService } from '../checkapp/diseases.service';
import { WikiService } from '../checkapp/wikis.service';

describe(MobileController, () => {
  let controller: MobileController;
  let diseaseSVC: DiseaseService;
  let wikiSVC: WikiService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      controllers: [MobileController],
      imports: [CheckappModule, AppModule],
    }).compile();

    controller = await module.resolve<MobileController>(MobileController);
    diseaseSVC = await module.resolve<DiseaseService>(DiseaseService);
    wikiSVC = await module.resolve<WikiService>(WikiService);
  });

  describe('profiling', () => {
    let basicFilteredDiseases: any[];

    beforeEach(() => {
      basicFilteredDiseases = [
        {
          name: 'Osteoporose',
          actions: [
            {
              name: 'Questionário FRAX',
              targets: [
                {
                  sex: 'female',
                  minAge: 45,
                  maxAge: 120,
                },
              ],
            },
          ],
        },
      ];

      jest
        .spyOn(diseaseSVC, 'filter')
        .mockResolvedValueOnce(basicFilteredDiseases);
    });

    afterEach(() => jest.clearAllMocks());

    it('should be defined', () => {
      expect(controller.profiling).toBeDefined();
    });

    it('repasses the filtered version of the database', async () => {
      const dto: any = { age: 52, sex: 'female' };
      expect(await controller.profiling(dto)).toEqual(basicFilteredDiseases);
    });
  });

  describe('wikis', () => {
    it('returns null if no wiki is found', async () => {
      jest.spyOn(wikiSVC, 'findOne').mockResolvedValueOnce(null);

      const wiki = await controller.findWiki('1234');

      expect(wiki).toBeNull();
    });
  });
});
