import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { Disease, DiseaseSchema } from './schemas/disease.schema';
import { Action, ActionSchema } from './schemas/action.schema';
import { Target, TargetSchema } from './schemas/target.schema';
import { Wiki, WikiSchema } from './schemas/wiki.schema';
import { Tab, TabSchema } from './schemas/tab.schema';
import { DiseaseService } from './diseases.service';
import { WikiService } from './wikis.service';

@Module({
  providers: [DiseaseService, WikiService],
  exports: [DiseaseService, WikiService],
  imports: [
    MongooseModule.forFeature([
      {
        name: Disease.name,
        schema: DiseaseSchema,
      },
      {
        name: Action.name,
        schema: ActionSchema,
      },
      {
        name: Target.name,
        schema: TargetSchema,
      },
      {
        name: Wiki.name,
        schema: WikiSchema,
      },
      {
        name: Tab.name,
        schema: TabSchema,
      },
    ]),
  ],
})
export class CheckappModule {}
