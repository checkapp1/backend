import { DiseaseService } from './diseases.service';
import { Test } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';

const db = [
  {
    name: 'Osteoporose',
    actions: [
      {
        name: 'Calculadora FRAX',
        targets: [
          {
            minAge: 45,
            maxAge: 120,
            sex: 'female',
          },
        ],
      },
      {
        name: 'Densitometria Óssea',
        targets: [
          {
            minAge: 45,
            maxAge: 120,
            sex: 'female',
          },
        ],
      },
    ],
  },
  {
    name: 'Câncer de Próstata',
    actions: [
      {
        name: 'Radioterapia',
        targets: [
          {
            sex: 'male',
            minAge: 50,
            maxAge: 75,
          },
        ],
      },
    ],
  },
];

describe(DiseaseService, () => {
  const mockDisease = { find: jest.fn() };
  let service: DiseaseService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        DiseaseService,
        {
          provide: getModelToken('Disease'),
          useValue: mockDisease,
        },
      ],
    }).compile();

    service = module.get<DiseaseService>(DiseaseService);

    mockDisease.find.mockResolvedValueOnce(db);
  });

  it('returns an empty array when no matches are found', async () => {
    const diseases = await service.filter({ sex: 'male', age: 20 });

    expect(diseases).toHaveLength(0);
  });

  it('returns a non-empty array when there are matches', async () => {
    const diseases = await service.filter({ sex: 'female', age: 70 });

    expect(diseases).toHaveLength(1);
  });
});
