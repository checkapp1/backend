import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

import { Tab } from '../interfaces/tab.interface';
import { TabSchema } from './tab.schema';

@Schema()
export class Wiki extends Document {
  @Prop({ required: true })
  name: string;

  @Prop([TabSchema])
  tabs: Tab[];
}

export const WikiSchema = SchemaFactory.createForClass(Wiki);
