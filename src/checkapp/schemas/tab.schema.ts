import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Tab extends Document {
  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  body: number;
}

export const TabSchema = SchemaFactory.createForClass(Tab);
