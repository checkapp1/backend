import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

import { Action } from '../interfaces/action.interface';
import { ActionSchema } from './action.schema';

@Schema()
export class Disease extends Document {
  @Prop({ required: true })
  name: string;

  @Prop([ActionSchema])
  actions: Action[];
}

export const DiseaseSchema = SchemaFactory.createForClass(Disease);
