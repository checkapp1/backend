import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

import { TargetSchema } from './target.schema';
import { Target } from '../interfaces/target.interface';

@Schema()
export class Action extends Document {
  @Prop({ required: true })
  name: string;

  @Prop()
  disclaimer: string;

  @Prop([TargetSchema])
  targets: Target[];
}

export const ActionSchema = SchemaFactory.createForClass(Action);
