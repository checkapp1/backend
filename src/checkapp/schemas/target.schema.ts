import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Target extends Document {
  @Prop({ required: true })
  sex: string;

  @Prop({ required: true })
  minAge: number;

  @Prop({ required: true })
  maxAge: number;
}

export const TargetSchema = SchemaFactory.createForClass(Target);
