import { Target } from './target.interface';

export interface Action {
  name: string;
  disclaimer: string;
  targets: Target[];
}
