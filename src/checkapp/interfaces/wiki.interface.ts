import { Tab } from './tab.interface';

export interface Wiki {
  name: string;
  tabs: Tab[];
}
