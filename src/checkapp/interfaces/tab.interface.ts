export interface Tab {
  name: string;
  body: string;
}
