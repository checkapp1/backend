import { Action } from './action.interface';

export interface Disease {
  name: string;
  actions: Action[];
}
