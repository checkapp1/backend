export interface Target {
  sex: string;
  minAge: number;
  maxAge: number;
}
