import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Disease } from './schemas/disease.schema';

@Injectable()
export class DiseaseService {
  constructor(@InjectModel(Disease.name) private Disease: Model<Disease>) {}

  async filter(dto: any): Promise<Disease[]> {
    const allDiseases = await this.Disease.find({});

    const diseases = allDiseases.filter(({ actions }) =>
      actions.some(({ targets }) =>
        targets.some(
          ({ sex, minAge, maxAge }) =>
            sex === dto.sex && minAge <= dto.age && dto.age <= maxAge,
        ),
      ),
    );

    return diseases;
  }

  async create(dto: any) {
    const { name } = dto;
    const disease = await this.Disease.findOne({ name });

    if (disease) {
      return null;
    } else {
      return await this.Disease.create({ name, actions: [] });
    }
  }
}
