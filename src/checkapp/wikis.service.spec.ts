import { WikiService } from './wikis.service';
import { Test } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';

const db = [
  {
    name: 'Calculadora FRAX',
    _id: '1',
    tabs: [
      {
        name: 'Resumo',
        body: 'Foo bar',
      },
    ],
  },
];

describe(WikiService, () => {
  const mockWiki = { find: jest.fn(), findById: jest.fn() };
  let service: WikiService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        WikiService,
        {
          provide: getModelToken('Wiki'),
          useValue: mockWiki,
        },
      ],
    }).compile();

    service = module.get<WikiService>(WikiService);

    mockWiki.find.mockResolvedValueOnce(db);
  });

  describe('allNames', () => {
    it('returns only names and ids', async () => {
      const wikis = await service.allNames();

      expect(wikis).toEqual([{ name: 'Calculadora FRAX', _id: '1' }]);
    });
  });

  describe('findOne', () => {
    it('returns the wiki, when it exists', async () => {
      mockWiki.findById.mockResolvedValueOnce({ name: 'Mocked Wiki' });

      const wiki = await service.findOne('1234');

      expect(wiki).not.toBeFalsy();
    });

    it('raises an exception when no wiki is found', async () => {
      mockWiki.findById.mockResolvedValueOnce(null);

      const wiki = await service.findOne('1234');

      expect(wiki).toBeNull();
    });
  });
});
