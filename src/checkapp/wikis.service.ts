import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Wiki } from './schemas/wiki.schema';

@Injectable()
export class WikiService {
  constructor(@InjectModel(Wiki.name) private Wiki: Model<Wiki>) {}

  async allNames(): Promise<any[]> {
    const wikis = await this.Wiki.find({});

    return wikis.map(({ _id, name }) => ({ _id, name }));
  }

  async findOne(id) {
    return await this.Wiki.findById(id);
  }
}
