import { Controller, Body, BadRequestException, Post } from '@nestjs/common';
import { DiseaseService } from '../checkapp/diseases.service';
import { DiseaseCreationDTO } from './dto/disease_creation.dto';

@Controller('curation')
export class CurationController {
  constructor(private diseasesSVC: DiseaseService) {}

  @Post('/diseases')
  async create(@Body() dto: DiseaseCreationDTO) {
    const disease = await this.diseasesSVC.create(dto);

    if (!disease) {
      throw new BadRequestException();
    } else {
      return disease;
    }
  }
}
