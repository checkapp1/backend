import { Test, TestingModule } from '@nestjs/testing';
import { CurationController } from './curation.controller';
import { CheckappModule } from '../checkapp/checkapp.module';
import { AppModule } from '../app.module';

describe('CurationController', () => {
  let controller: CurationController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CurationController],
      imports: [CheckappModule, AppModule],
    }).compile();

    controller = await module.resolve<CurationController>(CurationController);
  });

  it('is defined', () => expect(controller).toBeDefined());
});
