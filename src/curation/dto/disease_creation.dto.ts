import { IsString, Length } from 'class-validator';

export class DiseaseCreationDTO {
  @IsString()
  @Length(2)
  readonly name: string;
}
