import { Module } from '@nestjs/common';
import { CurationController } from './curation.controller';
import { CheckappModule } from '../checkapp/checkapp.module';

@Module({
  imports: [CheckappModule],
  controllers: [CurationController],
})
export class CurationModule {}
