import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CheckappModule } from './checkapp/checkapp.module';
import { MobileModule } from './mobile/mobile.module';
import { CurationModule } from './curation/curation.module';

const { MONGO_URL } = process.env;

@Module({
  imports: [MongooseModule.forRoot(MONGO_URL), CheckappModule, MobileModule, CurationModule],
  exports: [CheckappModule, MobileModule],
})
export class AppModule {}
