 # CheckApp Backend

## Up and Running

- `docker-compose build`
- `docker-compose up`

## Testing

- unity and integration `docker-compose run --rm app yarn test`
- interface/e2e `docker-compose run --rm app yarn test:e2e`
